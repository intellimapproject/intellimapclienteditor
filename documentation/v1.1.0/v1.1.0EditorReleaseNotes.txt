IntelliMap Editor
Version 1.1.0

date of update git 12/5/2017 by Or Amit.

Details:
- add documentation directory.
- fixing important bug in id index higher than 10 (wrong substring use).
- room id counter will start from the value of  1 (instead of 0).  (only Floors should use the pattern x.y.z.0).

