/**
 * Created by oramit on 3/15/2017.
 */

//const
const minimumRoomUnits=2;
let roomID_counter=1;
let domain="https://intelli-map-server.herokuapp.com";
//let domain="http://localhost:3000";
const borderStyle = "2px solid #aaaaaa"; //room border style

var floorSpaceUnits=[];

function printFloorObjectToConsole(){
    console.log("FloorSpaceUnits array:");
    console.log(floorSpaceUnits);
}
//printFloorObjectToConsole();


var isSiteCreated=false;
var isBuildingCreated=false;
var isFloorCreated=false;
var isNeedToDefineXY_POINT=false;
var isNeedToDefineEntrance_Point=false;
var isNeedToDefine_Elevator_XY_POINT=false;
var isNeedToDefine_Stairs_XY_POINT=false;
var isNeedToDeleteRoom=false;


var siteName;
var siteDescription;

var buildingName;
var buildingDescription;
var buildingWidth;
var buildingHeight;

var floorWidthVal;
var floorHeightVal;

var siteId;
var siteId_UI=document.getElementById("siteId");
var siteNameForBreadcrumb;

var buildingId;
var buildingId_UI= document.getElementById("buildingId");
var buildingNameForBreadcrumb;

var floorId;
var floorId_UI= document.getElementById("floorId");
var floorNumberForBreadcrumb;




var allSitesObjects;
var allBuildingsOfSite;
var allFloorsOfBuilding;
var allSpaceUnitsOfFloor;


/*UI Forms*/
var newSiteForm=document.getElementById("newSiteForm");
var newBuildingForm=document.getElementById("newBuildingForm");
var newFloorForm=document.getElementById("newFloorForm");
var newRoomForm=document.getElementById("newRoomForm");
var newElevatorForm=document.getElementById("newElevatorForm");
var newStairsForm=document.getElementById("newStairsForm");


/*UI ELEMENTS*/
var printObjToConsole=document.getElementById("printObjToConsole");
//printObjToConsole.style.display="none";

var saveBtn=document.getElementById("saveBtn");
saveBtn.style.display="none";

var clearMapBtn=document.getElementById("clearMap");
clearMapBtn.style.display="none";

var changeSiteBtn=document.getElementById("changeSiteBtn");
var userSelectedSiteDoneBtd=document.getElementById("userSelectedSiteDoneBtd");
userSelectedSiteDoneBtd.style.display="none";

var siteSelection=document.getElementById("siteSelection");
siteSelection.style.display="none";

var createBuildingBtn=document.getElementById("createBuilding");
createBuildingBtn.style.display="none";

var changeBuildingBtn=document.getElementById("changeBuildingBtn");
changeBuildingBtn.style.display="none";

var buildingSelection=document.getElementById("buildingSelection");
buildingSelection.style.display="none";

var userSelectedBuildingDoneBtd=document.getElementById("userSelectedBuildingDoneBtd");
userSelectedBuildingDoneBtd.style.display="none";



var createFloorBtn=document.getElementById("createfloor");
createFloorBtn.style.display="none";

var changefloorBtn=document.getElementById("changefloorBtn");
changefloorBtn.style.display="none";

var floorSelection=document.getElementById("floorSelection");
floorSelection.style.display="none";

var userSelectedfloorDoneBtd=document.getElementById("userSelectedfloorDoneBtd");
userSelectedfloorDoneBtd.style.display="none";



var createRoomBtn=document.getElementById("createRoomBtn");
 createRoomBtn.style.display="none";

var createToiletBtn=document.getElementById("createToiletBtn");
 createToiletBtn.style.display="none";


var addElevatorBtn=document.getElementById("addElevatorBtn");
addElevatorBtn.style.display="none";
var doneElevatorAddingBtn=document.getElementById("doneElevatorAddingBtn");

var addStairsBtn=document.getElementById("addStairsBtn");
addStairsBtn.style.display="none";
var doneStairsAddingBtn=document.getElementById("doneStairsAddingBtn");


var mapUnitsSectionTools=document.getElementById("mapUnitsSectionTools");
mapUnitsSectionTools.style.display="none";

//----------------------------------------------------------------------




//----------------------------MAP UI ------------------------------
//one unit size
const unitWidth  = 52;
const unitHeight = 52;

var topLeftCellX;
var topLeftCellY;
var entranceCellX;
var entranceCellY;
var entranceDirection;

const mapSection  = document.getElementById("mapSection");


function closeAllForms() {
    addElevatorBtn.style.display="none";
    addStairsBtn.style.display="none";

    createRoomBtn.style.display="none";
    createToiletBtn.style.display="none";

    console.log("closeAllForms");
    newSiteForm.style.display="none";
    newBuildingForm.style.display="none";
    newFloorForm.style.display="none";
    newRoomForm.style.display="none";
    newElevatorForm.style.display="none";
    newStairsForm.style.display="none";

    siteSelection.style.display="none";
    userSelectedSiteDoneBtd.style.display="none";

    floorSelection.style.display="none";
    userSelectedfloorDoneBtd.style.display="none";

    buildingSelection.style.display="none";
    userSelectedfloorDoneBtd.style.display="none";
}
closeAllForms();


function clearMap(){
    console.log("clearMap");
    var mapSection = document.getElementById("mapSection");
    while (mapSection.firstChild) {
        mapSection.removeChild(mapSection.firstChild);
    }
    floorSpaceUnits=[];
    mapUnitsSectionTools.style.display="none";
}
function clearMapAndDrawFloor() {
    console.log("clearMapAndDrawFloor");
    clearMap();
    createFloor(floorWidthVal,floorHeightVal);
}

function deleteRoom() {
    console.log("delete room function");
    isNeedToDeleteRoom=true;
}

function isValidRoomDimentions(floor_Width,foor_Height,room_Width,room_Height,room_x_Point,room_y_Point){
    var floorWidth=Number(floor_Width);
    var foorHeight=Number(foor_Height);
    var roomWidth=Number(room_Width);
    var roomHeight=Number(room_Height);
    var room_xPoint=Number(room_x_Point);
    var room_yPoint=Number(room_y_Point);
    if((room_xPoint+roomWidth<=floorWidth)&&(room_yPoint+roomHeight<=foorHeight)){
        return true;
    }
    else{
        return false;
    }
}
function isValidRoomPosition(room_Width,room_Height,room_x_Point,room_y_Point){
    var isValidRoomPosition=true;
    var cellType;
    var roomWidth=Number(room_Width);
    var roomHeight=Number(room_Height);
    var room_xPoint=Number(room_x_Point);
    var room_yPoint=Number(room_y_Point);
    for(var row = room_yPoint; row < room_yPoint + roomHeight;row++){
        for(var col = room_xPoint; col < room_xPoint+roomWidth;col++){
            var cell =  document.getElementById("x"+col+"y"+row);
            cellType=cell.getAttribute("type");
            if(cellType!="corridor"){
                isValidRoomPosition=false;
            }
        }
    }
    return isValidRoomPosition;

}
function isValidElevatorOrStairsPosition(x_Point,y_Point,entranceDirection){
    var isValid=true;
    console.log("isValidElevatorOrStairsPosition");
    var elevatorStairs_FrontNeighbor_cell;
    var elevatorStairs_FrontNeighbor_cell_type;

    var x_Point=Number(x_Point);
    var y_Point=Number(y_Point);

    if (entranceDirection=="left"){
        console.log("left:");
        elevatorStairs_FrontNeighbor_cell=document.getElementById("x"+(x_Point-1)+"y"+y_Point);
        if(elevatorStairs_FrontNeighbor_cell==null){
            console.log("elevatorStairs_FrontNeighbor_cell is NULL");
            alert("elevator entrance turns to floor wall. must be directed towards a corridor ");
            isValid=false;
        }
        else{
            console.log("elevatorStairs_FrontNeighbor_cell:");
            console.log(+elevatorStairs_FrontNeighbor_cell);
            elevatorStairs_FrontNeighbor_cell_type=elevatorStairs_FrontNeighbor_cell.getAttribute("type");
            console.log("the FrontNeighbor TYPE is:"+elevatorStairs_FrontNeighbor_cell_type);
            if(elevatorStairs_FrontNeighbor_cell_type!="corridor"){
                alert("The elevator entrance must be directed towards a corridor ");
                isValid=false;
            }
        }
        //elevatorStairs_FrontNeighbor_cell_type=elevatorStairs_FrontNeighbor_cell.getAttribute("id");
    }
    else if (entranceDirection=="right"){
        console.log("right:");
        elevatorStairs_FrontNeighbor_cell=document.getElementById("x"+(x_Point+1)+"y"+y_Point);
        if(elevatorStairs_FrontNeighbor_cell==null){
            console.log("elevatorStairs_FrontNeighbor_cell is NULL");
            alert("elevator entrance turns to floor wall. must be directed towards a corridor ");
            isValid=false;
        }
        else{
            console.log("elevatorStairs_FrontNeighbor_cell:");
            console.log(elevatorStairs_FrontNeighbor_cell);
            elevatorStairs_FrontNeighbor_cell_type=elevatorStairs_FrontNeighbor_cell.getAttribute("type");
            console.log("the FrontNeighbor TYPE is:"+elevatorStairs_FrontNeighbor_cell_type);
            if(elevatorStairs_FrontNeighbor_cell_type!="corridor"){
                alert("The elevator entrance must be directed towards a corridor ");
                isValid=false;
            }
        }
    }
    else if (entranceDirection=="up"){
        console.log("up:");

        elevatorStairs_FrontNeighbor_cell=document.getElementById("x"+x_Point+"y"+(y_Point-1));
        if(elevatorStairs_FrontNeighbor_cell==null){
            console.log("elevatorStairs_FrontNeighbor_cell is NULL");
            alert("elevator entrance turns to floor wall. must be directed towards a corridor ");
            isValid=false;
        }
        else{
            console.log("elevatorStairs_FrontNeighbor_cell:");
            console.log(elevatorStairs_FrontNeighbor_cell);
            elevatorStairs_FrontNeighbor_cell_type=elevatorStairs_FrontNeighbor_cell.getAttribute("type");
            console.log("the FrontNeighbor TYPE is:"+elevatorStairs_FrontNeighbor_cell_type);
            if(elevatorStairs_FrontNeighbor_cell_type!="corridor"){
                alert("The elevator entrance must be directed towards a corridor ");
                isValid=false;
            }
        }
    }
    else if (entranceDirection=="down"){
        console.log("down:");
        elevatorStairs_FrontNeighbor_cell=document.getElementById("x"+x_Point+"y"+(y_Point+1));
        if(elevatorStairs_FrontNeighbor_cell==null){
            console.log("elevatorStairs_FrontNeighbor_cell is NULL");
            alert("elevator entrance turns to floor wall. must be directed towards a corridor ");
            isValid=false;
        }
        else{
            console.log("elevatorStairs_FrontNeighbor_cell:");
            console.log(elevatorStairs_FrontNeighbor_cell);
            elevatorStairs_FrontNeighbor_cell_type=elevatorStairs_FrontNeighbor_cell.getAttribute("type");
            console.log("the FrontNeighbor TYPE is:"+elevatorStairs_FrontNeighbor_cell_type);
            if(elevatorStairs_FrontNeighbor_cell_type!="corridor"){
                alert("The elevator entrance must be directed towards a corridor ");
                isValid=false;
            }
        }
    }
    return isValid;
}


function clickOnSpaceUnit() {
    var cellId=this.getAttribute("id");
    var cellRoomID=this.getAttribute("name");
    var cellType=this.getAttribute("type");


    var yLocation=cellId.indexOf("y");



    console.log("clickOnSpaceUnit() cellId:"+cellId);
    console.log("clickOnSpaceUnit() cellRoomID:"+cellRoomID);
    console.log("clickOnSpaceUnit() cellType:"+cellType);



    console.log("isNeedToDefineXY_POINT:  "+isNeedToDefineXY_POINT);
    console.log("isNeedToDefineEntrance_Point:  "+isNeedToDefineEntrance_Point);
    console.log("isNeedToDefine_Elevator_XY_POINT:  "+isNeedToDefine_Elevator_XY_POINT);
    console.log("isNeedToDefine_Stairs_XY_POINT:  "+isNeedToDefine_Stairs_XY_POINT);
    console.log("isNeedToDeleteRoom:  "+isNeedToDeleteRoom);


    if(isNeedToDeleteRoom==true&&cellType!="corridor"){
        deleteRoomFromScreenAndFromInnerDB(cellRoomID);
        isNeedToDeleteRoom=false;
    }
    else if(isNeedToDefineXY_POINT==true){
        topLeftCellX = Number(cellId.substring(1, yLocation));
        topLeftCellY = Number(cellId.substring(yLocation+1, cellId.length));

        // topLeftCellX=cellId.substring(1,2);
        // topLeftCellY=cellId.substring(3,4);
        console.log("topLeftCellX X is: "+topLeftCellX);
        console.log("topLeftCellY Y is: "+topLeftCellY);

        var roomWidthVal=document.getElementById("roomWidth").value;
        var roomHeightVal=document.getElementById("roomHeight").value;

        var roomName=document.getElementById("roomName").value;
        console.log("roomName Is:"+roomName);

        if(roomName==""){
            alert("need to enter a room name");
        }
        else{ //user entered a name to the room
            var isValidRoomD=isValidRoomDimentions(floorWidthVal,floorHeightVal,roomWidthVal,roomHeightVal,topLeftCellX,topLeftCellY);
            console.log(" isValidRoom is:"+isValidRoomD);
            if(isValidRoomD==true){
                var isValidRoomPos=isValidRoomPosition(roomWidthVal,roomHeightVal,topLeftCellX,topLeftCellY);
                console.log("  isValidRoomPosition:"+isValidRoomPos);
                if(isValidRoomPos==true){ //room is place ok- no on other room/elevetor ect
                    document.getElementById("xyPoint").innerText="X point:"+topLeftCellX+ " Y point:"+topLeftCellY;
                    document.getElementById("EntrancePointFieldset").style.display="block";
                    drawRoomOnScreen(topLeftCellX,topLeftCellY,roomWidthVal,roomHeightVal,room_type_creation); //draw room with borders
                    document.getElementById("EntrancePointFieldset").style.display="block";
                }
                else{
                    alert("room position isnt good. placed on other map object");
                }

            }
            else{
                alert("room xy cant be here. out of map drawing")
            }

        }
    }
    else if(isNeedToDefineEntrance_Point==true){
        entranceCellX=Number(cellId.substring(1, yLocation));
        entranceCellY=Number(cellId.substring(yLocation+1, cellId.length));
        console.log("entranceCell X  is: "+topLeftCellX);
        console.log("entranceCell Y  is: "+topLeftCellY);

        var dirSelect=document.getElementById("directionSelect");
        entranceDirection=dirSelect.value;
        drawEntranceOfRoom(this,entranceDirection);
    }
    else if(isNeedToDefine_Elevator_XY_POINT==true) {
        entranceCellX=Number(cellId.substring(1, yLocation));
        entranceCellY=Number(cellId.substring(yLocation+1, cellId.length));

        var elevatorDirectionSelect=document.getElementById("elevatorDirectionSelect");
        entranceDirection=elevatorDirectionSelect.value;

        var isValidElevatorPos=isValidElevatorOrStairsPosition(entranceCellX,entranceCellY,entranceDirection);
        if(isValidElevatorPos==true){
            document.getElementById("elevatorEntrance_xy_Point").innerText="X point:"+entranceCellX+ " Y point:"+entranceCellY;
            doneElevatorAddingBtn.style.display="block";
        }
    }
    else if(isNeedToDefine_Stairs_XY_POINT==true) {
        entranceCellX=Number(cellId.substring(1, yLocation));
        entranceCellY=Number(cellId.substring(yLocation+1, cellId.length));
        var stairsDirectionSelect=document.getElementById("stairsDirectionSelect");
        entranceDirection=stairsDirectionSelect.value;
        var isValidStairsPos=isValidElevatorOrStairsPosition(entranceCellX,entranceCellY,entranceDirection);
        if(isValidStairsPos==true){
            document.getElementById("stairsEntrance_xy_Point").innerText="X point:"+entranceCellX+ " Y point:"+entranceCellY;
            doneStairsAddingBtn.style.display="block";
        }
    }

    console.log("-topLeftCellX"+topLeftCellX);
    console.log("-topLeftCellY"+topLeftCellX);
    console.log("entranceCellX"+entranceCellX);
    console.log("entranceCellY"+entranceCellY);
}

var isOldMark=false;
var old_topLeftCellX;
var old_topLeftCellY;
var old_roomWidthVal;
var old_roomHeightVal;
var oldClasses=[];
function mouseOverOnSpaceUnit() {

    if(isNeedToDefineXY_POINT) {
        var roomWidthVal = document.getElementById("roomWidth").value;
        var roomHeightVal = document.getElementById("roomHeight").value;

        roomWidthVal=Number(roomWidthVal);
        roomHeightVal=Number(roomHeightVal);

        console.log("roomWidthVal=" + roomWidthVal);
        console.log("roomHeightVal=" + roomHeightVal);

        var thisCellId = this.getAttribute("id");

        //this.className=this.className+" room";
        var thisCellId_test = this.getAttribute("id");
        var yLocation=thisCellId_test.indexOf("y");
        var topLeftCellX = Number(thisCellId_test.substring(1, yLocation));
        var topLeftCellY = Number(thisCellId_test.substring(yLocation+1, thisCellId_test.length));

        console.log("this cell id:  x=" + topLeftCellX + " y=" + topLeftCellY);

        if(isOldMark==false){
            for (var row = topLeftCellY; row < topLeftCellY + roomHeightVal; row++) {
                for (var col = topLeftCellX; col < topLeftCellX + roomWidthVal; col++) {
                    var cell = document.getElementById("x" + col + "y" + row);
                    console.log("----"+cell.className);
                    if(oldClasses!=null){
                        oldClasses.push(cell.className);
                    }
                    if(cell!=null){
                        cell.className = cell.className + " room";
                    }
                }
            }
            isOldMark=true;
        }
        else{ //there is a mark on the board

            //delete the old
            console.log("deleted old");
            //var classesArrLength=oldClasses.length;
            var counter=0
            for (var row = old_topLeftCellY; row < old_topLeftCellY + old_roomHeightVal; row++) {
                for (var col = old_topLeftCellX; col < old_topLeftCellX + old_roomWidthVal; col++) {
                    var cell = document.getElementById("x" + col + "y" + row);
                    if(cell!=null){
                        cell.className=oldClasses[counter];
                        counter++;
                        //cell.className = "mapUnit";
                    }
                }
            }

            //crete new mark
            oldClasses=[];
            for (var row = topLeftCellY; row < topLeftCellY + roomHeightVal; row++) {
                for (var col = topLeftCellX; col < topLeftCellX + roomWidthVal; col++) {
                    var cell = document.getElementById("x" + col + "y" + row);
                    if(cell!=null){
                        oldClasses.push(cell.className);
                        cell.className = cell.className + " room";
                    }
                }
            }
            isOldMark=true;
        }
        old_topLeftCellX=topLeftCellX;
        old_topLeftCellY=topLeftCellY;
        old_roomWidthVal=roomWidthVal;
        old_roomHeightVal=roomHeightVal;
    }
}

function createFloor(flourWidth,flourHeight) {
    clearMap();
    mapUnitsSectionTools.style.display="block";
    mapSection.style.width = flourWidth * unitWidth +"px";
    mapSection.style.height = flourHeight * unitHeight+"px";
    for(let i = 0;i<flourWidth;i++){
        for(let j = 0;j<flourHeight;j++){
            var newUnit = document.createElement("canvas");
            newUnit.className = "mapUnit";
            newUnit.addEventListener("click",clickOnSpaceUnit);
            newUnit.addEventListener("mouseover",mouseOverOnSpaceUnit);
            // newUnit.onmouseover=function () {
            //     newUnit.className
            // };
            // newUnit.onmouseout=function () {
            //     console.log("mouse");
            // };

            newUnit.style.width=unitWidth+"px"; //mnus 2 beacuse of the border in the two sides
            newUnit.style.height=unitWidth+"px";
            newUnit.id = "x"+j+"y"+i;
            mapSection.appendChild(newUnit);
            document.getElementById("x"+j+"y"+i).setAttribute("type","corridor");
        }
    }
    isFloorCreated=true;
}

function deleteRoomFromScreenAndFromInnerDB(room_id){
    console.log("deleteRoomFromScreenAndFromInnerDB:  room_Id="+room_id);
    //find the room in the inner array of rooms
    var wantedRoom=floorSpaceUnits.find(function (room) {
        return room.id == room_id
    });
    console.log("wanted room is:");
    console.log(wantedRoom);

    //checking the room dimensions, in order to delete them from the map.
    var wantedRoom_width=Number(wantedRoom.zones[0].widthUnit);
    var wantedRoom_height=Number(wantedRoom.zones[0].heightUnit);
    var topLeft_X=Number(wantedRoom.zones[0].topLeftX);
    var topLeft_Y=Number(wantedRoom.zones[0].topLeftY);

    console.log("wantedRoom_width"+wantedRoom_width);
    console.log("wantedRoom_height"+wantedRoom_height);

    //map delete (using corridor draw)
    drawRoomOnScreen(topLeft_X,topLeft_Y,wantedRoom_width,wantedRoom_height,"corridor");

    //delete the room from inner array of rooms
    floorSpaceUnits = floorSpaceUnits.filter(function(room) { return room.id !== room_id; });
}


function drawRoomOnScreen(topLeft_X,topLeft_Y,room_Width,room_Height,type){
    isNeedToDefineXY_POINT=false;
    isNeedToDefineEntrance_Point=true;

    var topLeftX=Number(topLeft_X);
    var topLeftY=Number(topLeft_Y);
    var roomWidth=Number(room_Width);
    var roomHeight=Number(room_Height);

    console.log("drawRoomOnScreen");
    for(var row = topLeftY; row < topLeftY + roomHeight;row++){
        for(var col = topLeftX; col < topLeftX+roomWidth;col++){
            //console.log("x"+col+"y"+row);
            var cell =  document.getElementById("x"+col+"y"+row);

            if(col == topLeftX||col==(topLeftX+roomWidth-1)||row == topLeftY||(row == (topLeftY+roomHeight-1))){
                if(col == topLeftX){//left
                    cell.style["borderLeft"] = borderStyle;
                    if(type=="corridor"){
                        cell.style["borderLeft"] = "none";
                    }
                }
                if(col==(topLeftX+roomWidth-1)){//right
                    cell.style["borderRight"] = borderStyle;
                    if(type=="corridor"){ //for deletion
                        cell.style["borderRight"] = "none";
                    }

                }
                if(row == topLeftY){//up
                    cell.style["borderTop"] = borderStyle;
                    if(type=="corridor"){ //for deletion
                        cell.style["borderTop"] = "none";
                    }
                }
                if(row == (topLeftY+roomHeight-1)){//down
                    cell.style["borderBottom"] = borderStyle;
                    if(type=="corridor"){ //for deletion
                        cell.style["borderBottom"] = "none";
                    }
                }
                if(type!="corridor"){
                    cell.setAttribute("validEntrance","true");
                }
            }
            if(type=="room"){
                //console.log("its room");
                cell.className=cell.className+" room";
            }
            else if(type=="toilet"){
                cell.className=cell.className+" toilet";
            }
            else if(type=="elevator"){
                //console.log("its elevator");
                cell.className=cell.className+" elevator";
            }
            else if(type=="stairs"){
                //console.log("its stairs");
                cell.className=cell.className+" stairs";
            }
            else if(type=="corridor"){
                //console.log("its stairs");
                cell.className="mapUnit";
            }
            cell.setAttribute("type",type);
            cell.setAttribute("name",roomID_counter);
        }
    }
}

function drawEntranceOfRoom(cell,entranceDirection){
    console.log("drawEntranceOfRoom: cell is:");
    //console.log(cell);
    var isValidEntrance=cell.getAttribute("validEntrance");
    console.log("****iValidEntrance:"+isValidEntrance);
    console.log("****entranceDirection:"+entranceDirection);


    if(isValidEntrance==null){
        alert("please choose a valid exit (next to cooridor");
    }
    else if(isValidEntrance=="true"){
        isNeedToDefineEntrance_Point=false;
        document.getElementById("Entrance_xy_Point").innerText="X point:"+entranceCellX+ " Y point:"+entranceCellY;
        document.getElementById("doneBtn1").style.display="block";


        switch(entranceDirection) {
            case "up":
                console.log("case: UP");
                cell.style.borderTop="none";
                cell.setAttribute("roomEntranceDirection","up"); //for connect the room to corridor at "generateCorridor" function
                break;
            case "down":
                console.log("case: down");
                cell.style.borderBottom="none";
                cell.setAttribute("roomEntranceDirection","down"); //for connect the room to corridor at "generateCorridor" function
                break;
            case "left":
                console.log("case: left");
                cell.style.borderLeft="none";
                cell.setAttribute("roomEntranceDirection","left"); //for connect the room to corridor at "generateCorridor" function
                break;
            case "right":
                console.log("case: right");
                cell.style.borderRight="none";
                cell.setAttribute("roomEntranceDirection","right"); //for connect the room to corridor at "generateCorridor" function
                break;
            default:
                break;
        }

    }
}

function createRoom(roomObject) {
    console.log("create room function.  room obj:");
    console.log(roomObject);

      //console.log(roomObject.zones[0]);

    //console.log("the sum of x: "+roomTopLeftX+"with width:"+roomWidth+"is:"+sum);

    var topLeftX=Number(roomObject.zones[0].topLeftX);
    var topLeftY=Number(roomObject.zones[0].topLeftY);
    var roomWidth=Number(roomObject.zones[0].widthUnit);
    var roomHeight=Number(roomObject.zones[0].heightUnit);

     console.log("topLeftX:"+topLeftX+",  topLeftY:"+topLeftY);
     console.log("roomWidth:"+roomWidth+", roomHeight:"+roomHeight);

    for(var i = topLeftY; i < topLeftY + roomHeight;i++){
        for(var j = topLeftX; j < topLeftX+roomWidth;j++){
            console.log("x"+j+"y"+i);
            var cell =  document.getElementById("x"+j+"y"+i);

            if(roomObject.type=="room"){
                console.log("drawing room");
                cell.className=cell.className+" room";
            }
            if(roomObject.type=="toilet"){
                console.log("drawing toilet");
                cell.className=cell.className+" toilet";
            }
            else if(roomObject.type=="elevator"){
                //console.log("its elevator");
                cell.className=cell.className+" elevator";
            }
            else if(roomObject.type=="stairs"){
                //console.log("its stairs");
                cell.className=cell.className+" stairs";
            }
            cell.setAttribute("type",roomObject.type);
            cell.setAttribute("name",roomObject.id);
        }
    }
}

function createBorder(i,j,room,forCellUnit,borderDirection) {
  //  console.log("border diirectionnnn:" +borderDirection);
    var neighborCell = document.getElementById("x"+j+"y"+i);

    if(null !== neighborCell){
        if(neighborCell.getAttribute("name") != room.id){
            forCellUnit.style[borderDirection] = borderStyle;
        }
    }
    else{
        forCellUnit.style[borderDirection] = borderStyle;
    }
}

function createBorder2(RoomCellUnit,borderDirection) {
            RoomCellUnit.style[borderDirection] = borderStyle;
}

function createRoomBorder(roomObject){
    console.log("create room Border function");
    console.log(roomObject);
    roomObject.zones.forEach(function (zone) {
        //console.log("the zone is:");
        //console.log(zone);
        var topLeftX=Number(zone.topLeftX);
        var topLeftY=Number(zone.topLeftY);
        var zoneWidth=Number(zone.widthUnit);
        var zoneHeight=Number(zone.heightUnit);
        for(let i = topLeftY; i < topLeftY + zoneHeight;i++){
            for(let j = topLeftX; j < topLeftX + zoneWidth;j++){
                //console.log("----");
                //console.log("i="+i+"j="+j);
                var unit =  document.getElementById("x"+j+"y"+i);
                //up
                createBorder((i-1),j,roomObject,unit,"borderTop");
                //left
                createBorder(i,(j-1),roomObject,unit,"borderLeft");
                //down
                createBorder((i+1),j,roomObject,unit,"borderBottom");
                //right
                createBorder(i,(j+1),roomObject,unit,"borderRight");
            }
        }
    });

    //drawing the room entrance
    if(roomObject.entrance !== undefined){
        roomObject.entrance.forEach(function (entrance) {
            var unit =  document.getElementById("x"+entrance.xExitPosition+"y"+entrance.yExitPosition);
            switch(entrance.direction) {
                case "left":
                    unit.style.borderLeft = "none";
                    unit.setAttribute("roomEntranceDirection","left"); //for connect the room to corridor at "generateCorridor" function
                    break;
                case "right":
                    unit.style.borderRight ="none";
                    unit.setAttribute("roomEntranceDirection","right"); //for connect the room to corridor at "generateCorridor" function
                    break;
                case "up":
                    unit.style.borderTop = "none";
                    unit.setAttribute("roomEntranceDirection","up"); //for connect the room to corridor at "generateCorridor" function
                    break;
                case "down":
                    unit.style.borderBottom = "none";
                    unit.setAttribute("roomEntranceDirection","down"); //for connect the room to corridor at "generateCorridor" function
                    break;
                default:
            }
        })
    }
}



//  ----OPEN OR CLOSE NEW OBJECTS FORMS FUNCTIONS----
function showNewSiteForm() {
    closeAllForms();
    console.log("showNewSiteForm function. opens the new site form");
    newSiteForm.style.display="block";
}

function closeNewSiteForm() {
    console.log("sclose the new site form");
    newSiteForm.style.display="none";
    if(isFloorCreated==true){
        addElevatorBtn.style.display="block";
        addStairsBtn.style.display="block";
        createRoomBtn.style.display="block";
        createToiletBtn.style.display="block";
    }
}

function showNewBuildingForm(){
    closeAllForms();
    console.log("showNewBuilding function. opens new building form");
    newBuildingForm.style.display="block";
}

function closeNewBuildingForm() {
    console.log("close the new site form");
    newBuildingForm.style.display="none";
    if(isFloorCreated==true){
        addElevatorBtn.style.display="block";
        addStairsBtn.style.display="block";
        createRoomBtn.style.display="block";
        createToiletBtn.style.display="block";
    }
}

function showNewFloorForm(){
    addElevatorBtn.style.display="none";
    addStairsBtn.style.display="none";
    createRoomBtn.style.display="none";
    createToiletBtn.style.display="none";
    closeAllForms();
    newFloorForm.style.display="block";
}

function closeNewFloorForm() {
    console.log("close New Floor Form");
    newFloorForm.style.display="none";
    if(isFloorCreated==true){
        addElevatorBtn.style.display="block";
        addStairsBtn.style.display="block";
        createRoomBtn.style.display="block";
        createToiletBtn.style.display="block";
    }
}

var room_type_creation=undefined;
function showNewRoomForm(type){
    room_type_creation=type;

    console.log("showNewRoomForm, type of room: "+type);

    closeAllForms();
    addElevatorBtn.style.display="none";
    addStairsBtn.style.display="none";
    createRoomBtn.style.display="none";
    createToiletBtn.style.display="none";

    newRoomForm.style.display="block";
    var formTitle=document.getElementById("formTitleRoomToilet");

    if(type=="toilet"){
        formTitle.innerText="New Toilet";
    }
    else if(type=="room"){
        formTitle.innerText="New Room";
    }

    document.getElementById("doneBtn1").style.display="none";
    document.getElementById("EntrancePointFieldset").style.display="none";

    isNeedToDefineXY_POINT=true;

    document.getElementById("roomWidth").value="4";
    document.getElementById("roomHeight").value="3";

    document.getElementById("roomWidth").min=minimumRoomUnits;
    document.getElementById("roomHeight").min=minimumRoomUnits;

    document.getElementById("roomWidth").max=floorWidthVal;
    document.getElementById("roomHeight").max=floorHeightVal;
}
function closeNewRoomForm(){
    newRoomForm.style.display="none";
    addElevatorBtn.style.display="block";
    addStairsBtn.style.display="block";
    createRoomBtn.style.display="block";
    createToiletBtn.style.display="block";
    if(isNeedToDefineEntrance_Point==true){//user draw the room, but clicked cancell on the form
        //deleting the mark
        var roomWidthVal=document.getElementById("roomWidth").value;
        var roomHeightVal=document.getElementById("roomHeight").value;
        drawRoomOnScreen(topLeftCellX,topLeftCellY,roomWidthVal,roomHeightVal,"corridor");
    }

    isNeedToDefineXY_POINT=false;
    isNeedToDefineEntrance_Point=false;
}

function showNewElevatorForm(){
    closeAllForms();
    console.log("showNewElevatorForm");
    newElevatorForm.style.display="block";
    addElevatorBtn.style.display="none";
    addStairsBtn.style.display="none";
    isNeedToDefine_Elevator_XY_POINT=true;
    doneElevatorAddingBtn.style.display="none";
    createRoomBtn.style.display="none";
    createToiletBtn.style.display="none";
}
function closeNewElevatorForm(){
    newElevatorForm.style.display="none";
    addElevatorBtn.style.display="block";
    addStairsBtn.style.display="block";
    createRoomBtn.style.display="block";
    createToiletBtn.style.display="block";
    isNeedToDefine_Elevator_XY_POINT=false;
}

function showNewStairsForm(){
    closeAllForms();
    console.log("showNewStairsForm");
    newStairsForm.style.display="block";
    addElevatorBtn.style.display="none";
    addStairsBtn.style.display="none";
    isNeedToDefine_Stairs_XY_POINT=true;
    doneStairsAddingBtn.style.display="none";
    createRoomBtn.style.display="none";
    createToiletBtn.style.display="none";
}
function closeNewStairsForm(){
    newStairsForm.style.display="none";

    addElevatorBtn.style.display="block";
    addStairsBtn.style.display="block";
    isNeedToDefine_Stairs_XY_POINT=false;
    createRoomBtn.style.display="block";
    createToiletBtn.style.display="block";
}

//------------------------------------------


//  ----DONE ADDING OBJECTS FUNCTIONS----
function doneSiteAdding() { //clicking on done button on adding new site form
    buildingId=undefined;
    buildingId_UI.innerText="Building ⇨ undefined";
    buildingId_UI.className="spaceUnitMenueIds";

    floorId=undefined;
    floorId_UI.innerText="Floor ⇨ undefined";
    floorId_UI.className="spaceUnitMenueIds";
    saveBtn.style.display="none";
    clearMapBtn.style.display="none";



    createFloorBtn.style.display="none";
    changefloorBtn.style.display="none";

    floorSpaceUnits=[];
    clearMap();

    var serverResponse;
    isSiteCreated=true;
    siteName;
    siteDescription;
    console.log("done site adding");
    siteName=document.getElementById("siteName").value;
    siteDescription=document.getElementById("siteDescription").value;
    console.log(" site name sanded to server:"+siteName);
    console.log(" site description sanded to server:"+siteDescription);


    var myXMLhttpReq = new XMLHttpRequest(),
        method = "POST",
        url = domain+"/api/v1/mapProducer/site?siteName="+siteName+"&siteDescription="+siteDescription;
    myXMLhttpReq.open(method, url, true);
    //myXMLhttpReq.setRequestHeader("Content-type", "application/json");
    myXMLhttpReq.onreadystatechange = function() {

        if (myXMLhttpReq.readyState == XMLHttpRequest.DONE) {
            if (myXMLhttpReq.status === 200||myXMLhttpReq.status === 201) {
                console.log("the server response:");
                serverResponse=JSON.parse(myXMLhttpReq.responseText);
                console.log(serverResponse);
                console.log("the siteId got from the server is is: "+serverResponse.data._id);
                siteId=serverResponse.data._id;
                siteNameForBreadcrumb=serverResponse.data.name;

                //UPDATE Breadcrumb
                siteId_UI.innerText="Site ⇨ "+siteNameForBreadcrumb+"("+siteId+")";
                siteId_UI.className+=" spaceUnitMenueIdsBoldText";


                createBuildingBtn.style.display="block";
                document.getElementById("newSiteForm").style.display="none";
            }
            else {
                console.log("Error on site saving", myXMLhttpReq.statusText);
                alert("Error on site saving");
            }

        }
        //console.log("test 3");
    };
    myXMLhttpReq.send(); //updating the server with the new site and getting a site id from him/
}

function doneFloorAdding(){ //for floor or room creation
    floorSpaceUnits=[];
    isOldMark=false;

    console.log("doneFloorAdding CLICKED");
    floorWidthVal=document.getElementById("floorWidth").value;
    floorHeightVal=document.getElementById("floorHeight").value;

    console.log("floorWidthVal is:"+floorWidthVal);
    console.log("floorHeightVal is:"+floorHeightVal);

    var floorNumber=document.getElementById("floorNumber").value;

    createFloor(floorWidthVal,floorHeightVal); //drawing to the screen

    document.getElementById("newFloorForm").style.display="none";//close the new floor form



    document.getElementById("createfloor").style.display="block";
    createToiletBtn.style.display="block";
    createRoomBtn.style.display="block";

    console.log("sending floor object to server (259 line)");
    console.log(" floor number sanded to server:"+floorNumber);
    console.log(" floor buildingId sanded to server:"+buildingId);
    console.log(" floor width sanded to server:"+floorWidthVal);
    console.log(" floor height sanded to server:"+floorHeightVal);

    var myXMLhttpReq = new XMLHttpRequest(),
        method = "POST",
        url = domain+"/api/v1/mapProducer/floor?buildingId="+buildingId+"&floorNumber="+floorNumber+"&floorWidth="+floorWidthVal+"&floorHeight="+floorHeightVal;

    myXMLhttpReq.open(method, url, true);
    //myXMLhttpReq.setRequestHeader("Content-type", "application/json");
    myXMLhttpReq.onreadystatechange = function() {
        if (myXMLhttpReq.readyState == XMLHttpRequest.DONE) {
            if (myXMLhttpReq.status === 200||myXMLhttpReq.status === 201) {
                console.log("the server response from floor adding function: (207!!)");
                serverResponse=JSON.parse(myXMLhttpReq.responseText);
                console.log(serverResponse);
                console.log("the floorID got from the server is is: "+serverResponse.data._id);
                floorId=serverResponse.data._id;
                floorNumberForBreadcrumb=serverResponse.data.name;
                //updating the menu gui
                floorId_UI.innerText="Floor ⇨ "+floorNumberForBreadcrumb+"("+floorId+")";
                floorId_UI.className+=" spaceUnitMenueIdsBoldText";
                buildingId_UI.className="spaceUnitMenueIds";
            }
            else if(myXMLhttpReq.status === 409){
                alert("there is such a floor number already.");
            }
            else {
                    console.log("Error", myXMLhttpReq.statusText);
                    alert("Error on doneFloorAdding()");
            }
        }
    };
    myXMLhttpReq.send(); //updating the server with the new floor and getting a floor id from him/
    newFloorForm.style.display="none";
    changefloorBtn.style.display="block";
    addElevatorBtn.style.display="block";
    addStairsBtn.style.display="block";
    saveBtn.style.display="block";
    clearMapBtn.style.display="block";
}

function doneBuildingAdding(){
    floorSpaceUnits=[];

    floorId=undefined;
    floorId_UI.innerText="Floor ⇨ undefined";
    floorId_UI.className="spaceUnitMenueIds";
    siteId_UI.className="spaceUnitMenueIds";

    saveBtn.style.display="none";
    clearMapBtn.style.display="none";




    var serverResponse;
    isBuildingCreated=true;
    console.log("doneBuildingAdding");

    ////
    buildingName=document.getElementById("buildingName").value;
    buildingDescription=document.getElementById("buildingDescription").value;
    buildingWidth= document.getElementById("buildingWidth").value;
    buildingHeight=document.getElementById("buildingHeight").value;
    //var tempSiteId=6;

    console.log(" building name sanded to server:"+buildingName);
    console.log(" building description sanded to server:"+buildingDescription);
    console.log(" building width sanded to server:"+buildingWidth);
    console.log(" building height sanded to server:"+buildingHeight);
    console.log(" siteIs sanded to server:"+siteId);

    var myXMLhttpReq = new XMLHttpRequest(),
        method = "POST",
        url = domain+"/api/v1/mapProducer/building?siteId="+siteId+"&width="+buildingWidth+"&height="+buildingHeight+"&buildingName="+buildingName+"&buildingDescription="+buildingDescription;


    myXMLhttpReq.open(method, url, true);
    //myXMLhttpReq.setRequestHeader("Content-type", "application/json");
    myXMLhttpReq.onreadystatechange = function() {
        //console.log("test 1");
        if (myXMLhttpReq.readyState == XMLHttpRequest.DONE) {
            if (myXMLhttpReq.status === 200||myXMLhttpReq.status === 201) {
                console.log(myXMLhttpReq.responseText);
                console.log("the server response from add building function:");
                serverResponse=JSON.parse(myXMLhttpReq.responseText);
                console.log(serverResponse);
                console.log("the buildingID got from the server is is: "+serverResponse.data._id);
                buildingId=serverResponse.data._id;
                buildingNameForBreadcrumb=serverResponse.data.name;

                //updating the menu gui
                buildingId_UI.innerText="Building ⇨ "+buildingNameForBreadcrumb+"("+buildingId+")";
                buildingId_UI.className+=" spaceUnitMenueIdsBoldText";
                siteId_UI.className="spaceUnitMenueIds";
            }
            else {
                console.log("Error-doneBuildingAdding", myXMLhttpReq.statusText);
                alert("Error on doneBuildingAdding");
            }

        }
    };
    myXMLhttpReq.send(); //updating the server with the new site and getting a site id from him/


    document.getElementById("createfloor").style.display="block";
    document.getElementById("newBuildingForm").style.display="none";
    changeBuildingBtn.style.display="block";
    changefloorBtn.style.display="none";
    clearMap();
}

function doneRoomAdding(){
    oldClasses=[];
    isOldMark=false;

    console.log("*****doneRoomAdding*******");
    var roomWidthVal=document.getElementById("roomWidth").value;
    var roomHeightVal=document.getElementById("roomHeight").value;
    var dirSelect=document.getElementById("directionSelect");
    var roomName=document.getElementById("roomName").value;


    console.log("topLeftCellX: "+topLeftCellX);
    console.log("topLeftCellY: "+topLeftCellY);
    console.log("entranceCell_X: "+entranceCellX);
    console.log("entranceCell_Y: "+entranceCellY);



    console.log("room form direction is: "+dirSelect.value);
    entranceDirection=dirSelect.value;
    var roomObject={
        "id":roomID_counter,
        "name":roomName,
        "type":room_type_creation,
        "entrance":[
            {
                "xExitPosition":entranceCellX,
                "yExitPosition":entranceCellY,
                "direction":entranceDirection,
                "approachable": 0,
            }
        ],
        "zones":[{
            "topLeftX":topLeftCellX,
            "topLeftY":topLeftCellY,
            "widthUnit":roomWidthVal,
            "heightUnit":roomHeightVal
        },]
    }

    //adding the rum object to list
    floorSpaceUnits.push(roomObject);
    console.log("*****")
    printFloorObjectToConsole();

    topLeftCellX=undefined;
    topLeftCellY=undefined;
    entranceCellX=undefined;
    entranceCellY=undefined;


    //closing the roomAdding form
    newRoomForm.style.display="none";

    createRoomBtn.style.display="block";
    createToiletBtn.style.display="block";

    addElevatorBtn.style.display="block";
    addStairsBtn.style.display="block";

    roomID_counter++;
}

function doneElevatorAdding(){
    console.log("doneElevatorAdding()");
    var elevatorDirectionSelect=document.getElementById("elevatorDirectionSelect");
    entranceDirection=elevatorDirectionSelect.value;

    var elevatorConnectionSelect=document.getElementById("elevatorConnection");
    var elevatorConnectionName=elevatorConnectionSelect.value;
    console.log("elevatorConnectionName:"+elevatorConnectionName);

    var spaceUnitObject={
        "id":roomID_counter,
        "type":"elevator",
        "name":elevatorConnectionName,
        "entrance":[
            {
                "xExitPosition":entranceCellX,
                "yExitPosition":entranceCellY,
                "direction":entranceDirection,
                "approachable": 0,
            }
        ],
        "zones":[{
            "topLeftX":entranceCellX,
            "topLeftY":entranceCellY,
            "widthUnit":1,
            "heightUnit":1
        },]
    }
    createRoom(spaceUnitObject); //drawing the elevator to the screen
    createRoomBorder(spaceUnitObject);

    isNeedToDefine_Elevator_XY_POINT=false;


    //adding the elevator object to list
    floorSpaceUnits.push(spaceUnitObject);

    entranceCellX=undefined;
    entranceCellY=undefined;

    //closing the elevator Adding form
    newElevatorForm.style.display="none";



    addElevatorBtn.style.display="block";
    addStairsBtn.style.display="block";
    createRoomBtn.style.display="block";
    createToiletBtn.style.display="block";

    roomID_counter++;
}

function doneStairsAdding(){
    console.log("doneStairsAdding()");
    var stairsDirectionSelect=document.getElementById("stairsDirectionSelect");
    entranceDirection=stairsDirectionSelect.value;

    var stairsConnectionSelect=document.getElementById("stairsConnection");
    var stairsConnectionName=stairsConnectionSelect.value;
    console.log("elevatorConnectionName:"+stairsConnectionName);

    var spaceUnitObject={
        "id":roomID_counter,
        "type":"stairs",
        "name":stairsConnectionName,
        "entrance":[
            {
                "xExitPosition":entranceCellX,
                "yExitPosition":entranceCellY,
                "direction":entranceDirection,
                "approachable": 1,
            }
        ],
        "zones":[{
            "topLeftX":entranceCellX,
            "topLeftY":entranceCellY,
            "widthUnit":1,
            "heightUnit":1
        },]
    }
    createRoom(spaceUnitObject); //drawing the room to the screen
    createRoomBorder(spaceUnitObject);

    isNeedToDefine_Stairs_XY_POINT=false;

    //adding the stairs object to list
    floorSpaceUnits.push(spaceUnitObject);

    entranceCellX=undefined;
    entranceCellY=undefined;

    //closing the stairs Adding form
    newStairsForm.style.display="none";


    addStairsBtn.style.display="block";
    addElevatorBtn.style.display="block";
    createRoomBtn.style.display="block";
    createToiletBtn.style.display="block";

    roomID_counter++;

}


//------------------------------------------



//  ----CHANGE PATH FUNCTIONS----
function changeSiteBtnClick() {
    clearMap();
    console.log("changeSiteBtnClick site click");
    changeSiteBtn.style.display="none";
    siteSelection.style.display="block";
    getAllSiteList();
    userSelectedSiteDoneBtd.style.display="block";
}

function getAllSiteList() {
    while (siteSelection.firstChild) {
        siteSelection.removeChild(siteSelection.firstChild);
    }
    console.log("getAllSiteList");
    var myXMLhttpReq = new XMLHttpRequest(),
        method = "GET",
        url = domain+"/api/v1/mapConsumer/site";
    var serverResponse;
    myXMLhttpReq.open(method, url, true);
    //myXMLhttpReq.setRequestHeader("Content-type", "application/json");
    myXMLhttpReq.onreadystatechange = function() {
        if (myXMLhttpReq.readyState == XMLHttpRequest.DONE) {
            if (myXMLhttpReq.status === 200||myXMLhttpReq.status === 201) {
                console.log("the server response from get site function:");
                serverResponse=JSON.parse(myXMLhttpReq.responseText);
                console.log(serverResponse);
                console.log(serverResponse.data);
                allSitesObjects=serverResponse.data;
                allSitesObjects.forEach(function (site) {
                    console.log("name is:"+site.name);
                    var newSiteOption=document.createElement("option");
                    // var newSiteOption2=document.createElement("li");
                    newSiteOption.value=site.siteId;
                    newSiteOption.setAttribute("siteName",site.name);
                    newSiteOption.innerText=site.name+" :id("+site.siteId+")";
                    // newSiteOption2.innerText=site.name+" :id("+site.siteId+")";
                    siteSelection.appendChild(newSiteOption);
                    // document.getElementById(("siteSelection2")).appendChild(newSiteOption2);
                })
            }
            else {
                console.log("Error on function: getAllSiteList", myXMLhttpReq.statusText);
                alert("Error on function: getAllSiteList");
            }
        }
    };
    myXMLhttpReq.send();
}
function userSelectedSite(){
    buildingId=undefined;
    buildingId_UI.innerText="Building Id ⇨ undefined";
    buildingId_UI.className="spaceUnitMenueIds";

    floorId=undefined;
    floorId_UI.innerText="FloorId Id ⇨ undefined";
    floorId_UI.className="spaceUnitMenueIds";

    saveBtn.style.display="none";
    clearMapBtn.style.display="none";

    addElevatorBtn.style.display="none";
    addStairsBtn.style.display="none";
    createRoomBtn.style.display="none";
    createToiletBtn.style.display="none";

    floorSpaceUnits=[];



    siteSelection=document.getElementById("siteSelection");
    var userSelectionID = siteSelection.options[siteSelection.selectedIndex].value;
    console.log("the user selection of site is:"+userSelectionID);

    siteSelection.style.display="none";
    userSelectedSiteDoneBtd.style.display="none";
    changeSiteBtn.style.display="block";
    siteNameForBreadcrumb=siteSelection.options[siteSelection.selectedIndex].getAttribute("siteName");

    //update the Ids on menu
    siteId_UI.innerText="Site ⇨ "+siteNameForBreadcrumb+"("+userSelectionID+")";
    siteId_UI.className+=" spaceUnitMenueIdsBoldText";

    //update the editor siteID parameter
    siteId=userSelectionID;
    createBuildingBtn.style.display="block";

    //open the change building option
    changeBuildingBtn.style.display="block";

    createFloorBtn.style.display="none";
    changefloorBtn.style.display="none";

}



function changeBuildingBtnClick(){
    console.log("changeBuildingBtnClick click");
    if(siteId==undefined){
        alert("site isnt defined!");
    }
    else{
        changeBuildingBtn.style.display="none";
        buildingSelection.style.display="block";
        getAllBuildingList(siteId);
        userSelectedBuildingDoneBtd.style.display="block";
    }

}
function getAllBuildingList(site_Id){
    while (buildingSelection.firstChild) {
        buildingSelection.removeChild(buildingSelection.firstChild);
    }
    console.log("getting the building of site:"+site_Id);
    var myXMLhttpReq = new XMLHttpRequest(),
        method = "GET",
        url = domain+"/api/v1/mapConsumer/site/"+site_Id+"/buildings";
    var serverResponse;
    myXMLhttpReq.open(method, url, true);
    //myXMLhttpReq.setRequestHeader("Content-type", "application/json");
    myXMLhttpReq.onreadystatechange = function() {
        if (myXMLhttpReq.readyState == XMLHttpRequest.DONE) {
            if (myXMLhttpReq.status === 200||myXMLhttpReq.status === 201) {
                console.log("the server response is:");
                serverResponse=JSON.parse(myXMLhttpReq.responseText);
                console.log(serverResponse);

                allBuildingsOfSite=serverResponse.data;
                allBuildingsOfSite.forEach(function (building) {
                    console.log("building Name:"+building.name);
                    console.log("building ID:"+building.buildingId);
                    var newBuildingOption=document.createElement("option");
                    newBuildingOption.value=building.buildingId;
                    newBuildingOption.setAttribute("buildingName",building.name);
                    newBuildingOption.innerText=building.name+" :id("+building.buildingId+")";
                    buildingSelection.appendChild(newBuildingOption);

                })

            }
            else {
                console.log("Error:getAllBuildingList", myXMLhttpReq.statusText);
                alert("Error on getAllBuildingList");
            }
        }
    };
    myXMLhttpReq.send();
}
function userSelectedBuilding(){
    floorId=undefined;

    floorId_UI.innerText="Floor ⇨ undefined";
    floorId_UI.className="spaceUnitMenueIds";
    siteId_UI.className="spaceUnitMenueIds";

    floorSpaceUnits=[];
    clearMap();

    saveBtn.style.display="none";
    clearMapBtn.style.display="none";

    addElevatorBtn.style.display="none";
    addStairsBtn.style.display="none";
    createRoomBtn.style.display="none";
    createToiletBtn.style.display="none";



    var userSelectionID = buildingSelection.options[buildingSelection.selectedIndex].value;
    console.log("the user selection of building is is:"+userSelectionID);

    buildingSelection.style.display="none";
    userSelectedBuildingDoneBtd.style.display="none";
    changeBuildingBtn.style.display="block";

    buildingNameForBreadcrumb=buildingSelection.options[buildingSelection.selectedIndex].getAttribute("buildingName");
    //update the Ids on menu
    buildingId_UI.innerText="Building ⇨ "+buildingNameForBreadcrumb+"("+userSelectionID+")";
    buildingId_UI.className+=" spaceUnitMenueIdsBoldText";

    //update the editor siteID parameter
    buildingId=userSelectionID;
    document.getElementById("createfloor").style.display="block";
    // changefloorBtn.style.display="block";
     floorSelection.style.display="block";
     userSelectedfloorDoneBtd.style.display="block";
    getAllfloorList(buildingId);

}


function changefloorBtnClick(){
    console.log("changefloorBtnClick click");
   // if(siteId==undefined||floorId==undefined){
    //    alert("site/floorId isnt defined!");
   // }
   // else{
        changefloorBtn.style.display="none";
        floorSelection.style.display="block";
        getAllfloorList(buildingId);
        userSelectedfloorDoneBtd.style.display="block";
   // }

}
function getAllfloorList(building_Id){
    console.log("getAllfloorList");
    while (floorSelection.firstChild) {
        floorSelection.removeChild(floorSelection.firstChild);
    }

    console.log("getting the FLOORS of BUILDING:"+building_Id);
     var myXMLhttpReq = new XMLHttpRequest(),
         method = "GET",
         url = domain+"/api/v1/mapConsumer/building/"+building_Id+"/floors";
     var serverResponse;
     myXMLhttpReq.open(method, url, true);
     //myXMLhttpReq.setRequestHeader("Content-type", "application/json");
     myXMLhttpReq.onreadystatechange = function() {
          if (myXMLhttpReq.readyState == XMLHttpRequest.DONE) {
              if (myXMLhttpReq.status === 200||myXMLhttpReq.status === 201) {
                  console.log("the server response is:");
                  serverResponse=JSON.parse(myXMLhttpReq.responseText);
                  console.log(serverResponse);

                  allFloorsOfBuilding=serverResponse.data;
                  allFloorsOfBuilding.forEach(function (floor) {
                      console.log("floor Name (floor number):"+floor.name);
                      console.log("floor ID:"+floor.floorId);
                      var newfloorOption=document.createElement("option");
                      newfloorOption.value=floor.floorId;
                      newfloorOption.setAttribute("floorName",floor.name);
                      newfloorOption.innerText="floor number:"+floor.name;
                      floorSelection.appendChild(newfloorOption);

                  })
              }
              else {
                  console.log("Error:getAllfloorList", myXMLhttpReq.statusText);
                  alert("Error on getAllfloorList()");
              }
         }
    };
    myXMLhttpReq.send();
}
function userSelectedfloor() {
    floorSpaceUnits=[];
    isOldMark=false;

    console.log("userSelectedfloor");
    var userFloorSelectionId=floorSelection.options[floorSelection.selectedIndex].value;

    floorNumberForBreadcrumb=floorSelection.options[floorSelection.selectedIndex].getAttribute("floorName");
    console.log("userSelectedfloorId:"+userFloorSelectionId);
    // console.log(floorSelection.options[floorSelection.selectedIndex].value);
    console.log("list is");
    //console.log(allFloorsOfBuilding);


    var wantedFloor=allFloorsOfBuilding.find(function (floor) {
        return floor.floorId == userFloorSelectionId
    });

    console.log("Wanted floor is:");
    console.log(wantedFloor);
    floorWidthVal=wantedFloor.zones[0].heightUnit;
    floorHeightVal=wantedFloor.zones[0].widthUnit;
    createFloor(floorWidthVal,floorHeightVal); //drawing the floor (board) to screen

    floorId=wantedFloor.floorId;

    //update Breadcrumb
    floorId_UI.innerText="Floor ⇨ "+floorNumberForBreadcrumb+"("+floorId+")";
    floorId_UI.className+=" spaceUnitMenueIdsBoldText";
    buildingId_UI.className="spaceUnitMenueIds";


    //menu buttons handling
    floorSelection.style.display="none";
    userSelectedfloorDoneBtd.style.display="none";
    changefloorBtn.style.display="block";
    saveBtn.style.display="block";
    clearMapBtn.style.display="block";


    //bring all spaceUnits of the wanted floor
    var serverResponse;
    var myXMLhttpReq = new XMLHttpRequest(),
        method = "GET",
        url = domain+"/api/v1/mapConsumer/floor/"+floorId;
    myXMLhttpReq.open(method, url, true);
    //myXMLhttpReq.setRequestHeader("Content-type", "application/json");

    myXMLhttpReq.onreadystatechange = function() {
        if (myXMLhttpReq.readyState == XMLHttpRequest.DONE) {
            if (myXMLhttpReq.status === 200||myXMLhttpReq.status === 201) {
                console.log("the server response:");
                serverResponse=JSON.parse(myXMLhttpReq.responseText);
                console.log(serverResponse);
                var map=serverResponse.data.map;
                console.log("mapIs:");
                console.log(map);
                console.log("doing FOReACH:: *************")
                map.forEach(function (spaceUnit) {
                    // console.log("name is:"+spaceUnit.name);
                    // console.log("type is:"+spaceUnit.type);
                    if(spaceUnit.type!="corridor"){
                        console.log("creatnig space unit from type: "+spaceUnit.type);
                        //adding the room to objects
                        console.log(spaceUnit);
                        spaceUnit.id=spaceUnit.spaceUnitId;
                        createRoom(spaceUnit); //drawing the room to the screen
                        createRoomBorder(spaceUnit);
                        isNeedToDefineEntrance_Point=false;
                        isNeedToDefineXY_POINT=false;

                        //adding the rum object to list
                        floorSpaceUnits.push(spaceUnit);
                        roomID_counter++;
                    }
                })
            }
            else {
                console.log("Error-userSelectedfloor", myXMLhttpReq.statusText);
                alert("Error on userSelectedfloor");
            }
        }
    };
    myXMLhttpReq.send(); //updating
    addElevatorBtn.style.display="block";
    addStairsBtn.style.display="block";
    createRoomBtn.style.display="block";
    createToiletBtn.style.display="block";
}


//---------------------------------------------




//this function will mark the corridor elements on the map with a special attribute- "name: floorId+x,x",
function generateCorridorIds() {
    console.log("generateCorridorIds")
    for(let i = 0;i<floorWidthVal;i++){
        for(let j = 0;j<floorHeightVal;j++){
            let cell =  document.getElementById("x"+j+"y"+i);
            let type = cell.getAttribute('type');
            if(type === 'corridor'){
                console.log("testttttt");
                cell.setAttribute("name",floorId+"x"+j+"y"+i);
            }
        }
    }
}

function createCorridorEntrance(i,j,entranceArray,borderDirection){
    let neighborCell = document.getElementById("x"+j+"y"+i);
    if(null !== neighborCell){
        if(neighborCell.getAttribute('type') === 'corridor'){
            entranceArray.push({
                neighborId:neighborCell.getAttribute('name'),
                approachable:0
            });
        }
        else{
            //checking if the neighbor cell is room entrance
            if(neighborCell.style[borderDirection] === 'none'){
                entranceArray.push({
                    neighborId:neighborCell.getAttribute('name'),
                    approachable:0
                });
            }
        }
    }
}

function createCorridorObjects(i, j, corridorCell) {
    console.log("create corridor object")
    let type = corridorCell.getAttribute('type');
    let cellUnitId = corridorCell.getAttribute('name');
    let entranceArray = []; //will hold the corridor neighbor list

    //up
    createCorridorEntrance(i-1,j,entranceArray,"borderBottom");
    //left
    createCorridorEntrance(i,j-1,entranceArray,"borderRight");
    //down
    createCorridorEntrance(i+1,j,entranceArray,"borderTop");
    //right
    createCorridorEntrance(i,j+1,entranceArray,"borderLeft");
    //creating the corridor object and add it to dataMap.
    let corridor = {
        id:cellUnitId,
        zones:[{
            widthUnit:1,
            heightUnit:1,
            topLeftX:j,
            topLeftY:i,
        }],
        accessibility:1,
        parentId:floorId,
        type:'corridor',
        entrance:entranceArray
    };
    floorSpaceUnits.push(corridor);
}

function updateSpaceUnitNeighbors(i,j,cell) {
    console.log("updateSpaceUnitNeighbors");
    let cellUnitId = cell.getAttribute('name');
    let entrance = [];
    let roomEntranceDirection = cell.getAttribute('roomEntranceDirection');
    let neighborCell;
    switch (roomEntranceDirection){
        case "left":
            neighborCell = document.getElementById("x"+(j-1)+"y"+i);
            console.log("room direction cell:left");
            break;
        case "right":
            neighborCell = document.getElementById("x"+(j+1)+"y"+i);
            console.log("room direction cell:right");
            break;
        case "up":
            neighborCell = document.getElementById("x"+j+"y"+(i-1));
            console.log("room direction cell:up");
            break;
        case "down":
            neighborCell = document.getElementById("x"+j+"y"+(i+1));
            console.log("room direction cell:down");
            break;
        default:
    }
    if(neighborCell!==null && neighborCell!==undefined){
        entrance.push({
            neighborId:neighborCell.getAttribute('name'),
            approachable:0
        });
        console.log("new entrance created: ");
        console.log(entrance);


        //find the current room at the dataMap
        var spaceUnit = floorSpaceUnits.find(function (spaceUnitObject) {
            return spaceUnitObject.id == cellUnitId
        });
        console.log("-----------------------------------")
        console.log("cellUnitId:"+cellUnitId);
        console.log("spaceUnit:");
        console.log(spaceUnit);

        //connecting the room neighbor filed to the corridor object at dataMap
        if(spaceUnit !== null && spaceUnit !== undefined){
            console.log("t-111111");
            let targetEntrance = spaceUnit.entrance.find(function (entry) { //find the entrance in dataMap
                return entry.xExitPosition == j && entry.yExitPosition == i;

            });
            if(targetEntrance !== null && targetEntrance !== undefined){//successful result
                console.log("t-2222");
                console.log(targetEntrance);
                targetEntrance.neighborId = entrance[0].neighborId;//todo change array entrance to object
            }
        }
    }
}


// equivilant to "UpdateMapObject" on map testing
//this function will add to each room in dataMap his neighbor list.
//and in addition will create a corridor object with his neighbors details and add him to dataMap list of object.
function calculatingNeighbors(){
    console.log("user pressed save btn. calculatingNeighbors()");
    var floorSpaceUnitsFullList;

    /*
    floorSpaceUnitsFullList = [{
        "id": "247",
        "type": "room",
        "entrance": [{
            "xExitPosition": 2,
            "yExitPosition": 1,
            "neighborId":"54665x2y0",
            "direction": "up"
        }, {
            "xExitPosition": 1,
            "yExitPosition": 4,
            "neighborId":"54665x1y5",
            "direction": "down"
        }],
        "zones": [{
            "topLeftX": 1,
            "topLeftY": 1,
            "widthUnit": 3,
            "heightUnit": 2
        }, {
            "topLeftX": 1,
            "topLeftY": 3,
            "widthUnit": 1,
            "heightUnit": 2
        }]
    }, {
        "id": "246",
        "type": "room",
        "entrance": [{
            "xExitPosition": 3,
            "yExitPosition": 4,
            "neighborId":"54665x2y4",
            "direction": "left"
        }],
        "zones": [{
            "topLeftX": 3,
            "topLeftY": 4,
            "widthUnit": 4,
            "heightUnit": 2
        }]
    }, {
        "id": "245",
        "type": "room",
        "entrance": [{
            "xExitPosition": 5,
            "yExitPosition": 1,
            "neighborId":"54665x4y1",
            "direction": "left"
        }],
        "zones": [{
            "topLeftX": 5,
            "topLeftY": 1,
            "widthUnit": 2,
            "heightUnit": 2
        }]
    }, {
        "id": "2000",
        "type": "room",
        "entrance": [{
            "xExitPosition": 1,
            "yExitPosition": 7,
            "neighborId":"54665x1y6",
            "direction": "up"
        }],
        "zones": [{
            "topLeftX": 1,
            "topLeftY": 7,
            "widthUnit": 4,
            "heightUnit": 2
        }]
    }, {
        "id": "2001",
        "type": "room",
        "entrance": [{
            "xExitPosition": 6,
            "yExitPosition": 7,
            "neighborId":"54665x6y6",
            "direction": "up"
        }],
        "zones": [{
            "topLeftX": 6,
            "topLeftY": 7,
            "widthUnit": 3,
            "heightUnit": 3
        }]
    }, {
        "id": "3445",
        "type": "elevator",
        "entrance": [{
            "xExitPosition": 0,
            "yExitPosition": 6,
            "neighborId":"54665x1y6",
            "direction": "right"
        }],
        "zones": [{
            "topLeftX": 0,
            "topLeftY": 6,
            "widthUnit": 1,
            "heightUnit": 1
        }]
    }, {
        "id": "54665x0y0",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 0,
            "topLeftY": 0
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x0y1",
            "approachable": 1
        }, {
            "neighborId": "54665x1y0",
            "approachable": 1
        }]
    }, {
        "id": "54665x1y0",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 1,
            "topLeftY": 0
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x0y0",
            "approachable": 1
        }, {
            "neighborId": "54665x2y0",
            "approachable": 1
        }]
    }, {
        "id": "54665x2y0",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 2,
            "topLeftY": 0
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x1y0",
            "approachable": 1
        }, {
            "neighborId": "247",
            "approachable": 1
        }, {
            "neighborId": "54665x3y0",
            "approachable": 1
        }]
    }, {
        "id": "54665x3y0",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 3,
            "topLeftY": 0
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x2y0",
            "approachable": 1
        }, {
            "neighborId": "54665x4y0",
            "approachable": 1
        }]
    }, {
        "id": "54665x4y0",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 4,
            "topLeftY": 0
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x3y0",
            "approachable": 1
        }, {
            "neighborId": "54665x4y1",
            "approachable": 1
        }, {
            "neighborId": "54665x5y0",
            "approachable": 1
        }]
    }, {
        "id": "54665x5y0",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 5,
            "topLeftY": 0
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x4y0",
            "approachable": 1
        }, {
            "neighborId": "54665x6y0",
            "approachable": 1
        }]
    }, {
        "id": "54665x6y0",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 6,
            "topLeftY": 0
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x5y0",
            "approachable": 1
        }, {
            "neighborId": "54665x7y0",
            "approachable": 1
        }]
    }, {
        "id": "54665x7y0",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 7,
            "topLeftY": 0
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x6y0",
            "approachable": 1
        }, {
            "neighborId": "54665x7y1",
            "approachable": 1
        }, {
            "neighborId": "54665x8y0",
            "approachable": 1
        }]
    }, {
        "id": "54665x8y0",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 8,
            "topLeftY": 0
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x7y0",
            "approachable": 1
        }, {
            "neighborId": "54665x8y1",
            "approachable": 1
        }, {
            "neighborId": "54665x9y0",
            "approachable": 1
        }]
    }, {
        "id": "54665x9y0",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 9,
            "topLeftY": 0
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x8y0",
            "approachable": 1
        }, {
            "neighborId": "54665x9y1",
            "approachable": 1
        }]
    }, {
        "id": "54665x0y1",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 0,
            "topLeftY": 1
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x0y0",
            "approachable": 1
        }, {
            "neighborId": "54665x0y2",
            "approachable": 1
        }]
    }, {
        "id": "54665x4y1",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 4,
            "topLeftY": 1
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x4y0",
            "approachable": 1
        }, {
            "neighborId": "54665x4y2",
            "approachable": 1
        }, {
            "neighborId": "245",
            "approachable": 1
        }]
    }, {
        "id": "54665x7y1",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 7,
            "topLeftY": 1
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x7y0",
            "approachable": 1
        }, {
            "neighborId": "54665x7y2",
            "approachable": 1
        }, {
            "neighborId": "54665x8y1",
            "approachable": 1
        }]
    }, {
        "id": "54665x8y1",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 8,
            "topLeftY": 1
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x8y0",
            "approachable": 1
        }, {
            "neighborId": "54665x7y1",
            "approachable": 1
        }, {
            "neighborId": "54665x8y2",
            "approachable": 1
        }, {
            "neighborId": "54665x9y1",
            "approachable": 1
        }]
    }, {
        "id": "54665x9y1",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 9,
            "topLeftY": 1
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x9y0",
            "approachable": 1
        }, {
            "neighborId": "54665x8y1",
            "approachable": 1
        }, {
            "neighborId": "54665x9y2",
            "approachable": 1
        }]
    }, {
        "id": "54665x0y2",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 0,
            "topLeftY": 2
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x0y1",
            "approachable": 1
        }, {
            "neighborId": "54665x0y3",
            "approachable": 1
        }]
    }, {
        "id": "54665x4y2",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 4,
            "topLeftY": 2
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x4y1",
            "approachable": 1
        }, {
            "neighborId": "54665x4y3",
            "approachable": 1
        }]
    }, {
        "id": "54665x7y2",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 7,
            "topLeftY": 2
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x7y1",
            "approachable": 1
        }, {
            "neighborId": "54665x7y3",
            "approachable": 1
        }, {
            "neighborId": "54665x8y2",
            "approachable": 1
        }]
    }, {
        "id": "54665x8y2",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 8,
            "topLeftY": 2
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x8y1",
            "approachable": 1
        }, {
            "neighborId": "54665x7y2",
            "approachable": 1
        }, {
            "neighborId": "54665x8y3",
            "approachable": 1
        }, {
            "neighborId": "54665x9y2",
            "approachable": 1
        }]
    }, {
        "id": "54665x9y2",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 9,
            "topLeftY": 2
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x9y1",
            "approachable": 1
        }, {
            "neighborId": "54665x8y2",
            "approachable": 1
        }, {
            "neighborId": "54665x9y3",
            "approachable": 1
        }]
    }, {
        "id": "54665x0y3",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 0,
            "topLeftY": 3
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x0y2",
            "approachable": 1
        }, {
            "neighborId": "54665x0y4",
            "approachable": 1
        }]
    }, {
        "id": "54665x2y3",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 2,
            "topLeftY": 3
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x2y4",
            "approachable": 1
        }, {
            "neighborId": "54665x3y3",
            "approachable": 1
        }]
    }, {
        "id": "54665x3y3",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 3,
            "topLeftY": 3
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x2y3",
            "approachable": 1
        }, {
            "neighborId": "54665x4y3",
            "approachable": 1
        }]
    }, {
        "id": "54665x4y3",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 4,
            "topLeftY": 3
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x4y2",
            "approachable": 1
        }, {
            "neighborId": "54665x3y3",
            "approachable": 1
        }, {
            "neighborId": "54665x5y3",
            "approachable": 1
        }]
    }, {
        "id": "54665x5y3",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 5,
            "topLeftY": 3
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x4y3",
            "approachable": 1
        }, {
            "neighborId": "54665x6y3",
            "approachable": 1
        }]
    }, {
        "id": "54665x6y3",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 6,
            "topLeftY": 3
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x5y3",
            "approachable": 1
        }, {
            "neighborId": "54665x7y3",
            "approachable": 1
        }]
    }, {
        "id": "54665x7y3",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 7,
            "topLeftY": 3
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x7y2",
            "approachable": 1
        }, {
            "neighborId": "54665x6y3",
            "approachable": 1
        }, {
            "neighborId": "54665x7y4",
            "approachable": 1
        }, {
            "neighborId": "54665x8y3",
            "approachable": 1
        }]
    }, {
        "id": "54665x8y3",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 8,
            "topLeftY": 3
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x8y2",
            "approachable": 1
        }, {
            "neighborId": "54665x7y3",
            "approachable": 1
        }, {
            "neighborId": "54665x8y4",
            "approachable": 1
        }, {
            "neighborId": "54665x9y3",
            "approachable": 1
        }]
    }, {
        "id": "54665x9y3",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 9,
            "topLeftY": 3
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x9y2",
            "approachable": 1
        }, {
            "neighborId": "54665x8y3",
            "approachable": 1
        }, {
            "neighborId": "54665x9y4",
            "approachable": 1
        }]
    }, {
        "id": "54665x0y4",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 0,
            "topLeftY": 4
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x0y3",
            "approachable": 1
        }, {
            "neighborId": "54665x0y5",
            "approachable": 1
        }]
    }, {
        "id": "54665x2y4",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 2,
            "topLeftY": 4
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x2y3",
            "approachable": 1
        }, {
            "neighborId": "54665x2y5",
            "approachable": 1
        }, {
            "neighborId": "246",
            "approachable": 1
        }]
    }, {
        "id": "54665x7y4",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 7,
            "topLeftY": 4
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x7y3",
            "approachable": 1
        }, {
            "neighborId": "54665x7y5",
            "approachable": 1
        }, {
            "neighborId": "54665x8y4",
            "approachable": 1
        }]
    }, {
        "id": "54665x8y4",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 8,
            "topLeftY": 4
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x8y3",
            "approachable": 1
        }, {
            "neighborId": "54665x7y4",
            "approachable": 1
        }, {
            "neighborId": "54665x8y5",
            "approachable": 1
        }, {
            "neighborId": "54665x9y4",
            "approachable": 1
        }]
    }, {
        "id": "54665x9y4",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 9,
            "topLeftY": 4
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x9y3",
            "approachable": 1
        }, {
            "neighborId": "54665x8y4",
            "approachable": 1
        }, {
            "neighborId": "54665x9y5",
            "approachable": 1
        }]
    }, {
        "id": "54665x0y5",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 0,
            "topLeftY": 5
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x0y4",
            "approachable": 1
        }, {
            "neighborId": "54665x1y5",
            "approachable": 1
        }]
    }, {
        "id": "54665x1y5",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 1,
            "topLeftY": 5
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "247",
            "approachable": 1
        }, {
            "neighborId": "54665x0y5",
            "approachable": 1
        }, {
            "neighborId": "54665x1y6",
            "approachable": 1
        }, {
            "neighborId": "54665x2y5",
            "approachable": 1
        }]
    }, {
        "id": "54665x2y5",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 2,
            "topLeftY": 5
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x2y4",
            "approachable": 1
        }, {
            "neighborId": "54665x1y5",
            "approachable": 1
        }, {
            "neighborId": "54665x2y6",
            "approachable": 1
        }]
    }, {
        "id": "54665x7y5",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 7,
            "topLeftY": 5
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x7y4",
            "approachable": 1
        }, {
            "neighborId": "54665x7y6",
            "approachable": 1
        }, {
            "neighborId": "54665x8y5",
            "approachable": 1
        }]
    }, {
        "id": "54665x8y5",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 8,
            "topLeftY": 5
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x8y4",
            "approachable": 1
        }, {
            "neighborId": "54665x7y5",
            "approachable": 1
        }, {
            "neighborId": "54665x8y6",
            "approachable": 1
        }, {
            "neighborId": "54665x9y5",
            "approachable": 1
        }]
    }, {
        "id": "54665x9y5",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 9,
            "topLeftY": 5
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x9y4",
            "approachable": 1
        }, {
            "neighborId": "54665x8y5",
            "approachable": 1
        }, {
            "neighborId": "54665x9y6",
            "approachable": 1
        }]
    }, {
        "id": "54665x1y6",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 1,
            "topLeftY": 6
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x1y5",
            "approachable": 1
        }, {
            "neighborId": "3445",
            "approachable": 1
        }, {
            "neighborId": "2000",
            "approachable": 1
        }, {
            "neighborId": "54665x2y6",
            "approachable": 1
        }]
    }, {
        "id": "54665x2y6",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 2,
            "topLeftY": 6
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x2y5",
            "approachable": 1
        }, {
            "neighborId": "54665x1y6",
            "approachable": 1
        }, {
            "neighborId": "54665x3y6",
            "approachable": 1
        }]
    }, {
        "id": "54665x3y6",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 3,
            "topLeftY": 6
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x2y6",
            "approachable": 1
        }, {
            "neighborId": "54665x4y6",
            "approachable": 1
        }]
    }, {
        "id": "54665x4y6",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 4,
            "topLeftY": 6
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x3y6",
            "approachable": 1
        }, {
            "neighborId": "54665x5y6",
            "approachable": 1
        }]
    }, {
        "id": "54665x5y6",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 5,
            "topLeftY": 6
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x4y6",
            "approachable": 1
        }, {
            "neighborId": "54665x5y7",
            "approachable": 1
        }, {
            "neighborId": "54665x6y6",
            "approachable": 1
        }]
    }, {
        "id": "54665x6y6",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 6,
            "topLeftY": 6
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x5y6",
            "approachable": 1
        }, {
            "neighborId": "2001",
            "approachable": 1
        }, {
            "neighborId": "54665x7y6",
            "approachable": 1
        }]
    }, {
        "id": "54665x7y6",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 7,
            "topLeftY": 6
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x7y5",
            "approachable": 1
        }, {
            "neighborId": "54665x6y6",
            "approachable": 1
        }, {
            "neighborId": "54665x8y6",
            "approachable": 1
        }]
    }, {
        "id": "54665x8y6",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 8,
            "topLeftY": 6
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x8y5",
            "approachable": 1
        }, {
            "neighborId": "54665x7y6",
            "approachable": 1
        }, {
            "neighborId": "54665x9y6",
            "approachable": 1
        }]
    }, {
        "id": "54665x9y6",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 9,
            "topLeftY": 6
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x9y5",
            "approachable": 1
        }, {
            "neighborId": "54665x8y6",
            "approachable": 1
        }, {
            "neighborId": "54665x9y7",
            "approachable": 1
        }]
    }, {
        "id": "54665x0y7",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 0,
            "topLeftY": 7
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x0y8",
            "approachable": 1
        }]
    }, {
        "id": "54665x5y7",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 5,
            "topLeftY": 7
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x5y6",
            "approachable": 1
        }, {
            "neighborId": "54665x5y8",
            "approachable": 1
        }]
    }, {
        "id": "54665x9y7",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 9,
            "topLeftY": 7
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x9y6",
            "approachable": 1
        }, {
            "neighborId": "54665x9y8",
            "approachable": 1
        }]
    }, {
        "id": "54665x0y8",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 0,
            "topLeftY": 8
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x0y7",
            "approachable": 1
        }, {
            "neighborId": "54665x0y9",
            "approachable": 1
        }]
    }, {
        "id": "54665x5y8",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 5,
            "topLeftY": 8
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x5y7",
            "approachable": 1
        }, {
            "neighborId": "54665x5y9",
            "approachable": 1
        }]
    }, {
        "id": "54665x9y8",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 9,
            "topLeftY": 8
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x9y7",
            "approachable": 1
        }, {
            "neighborId": "54665x9y9",
            "approachable": 1
        }]
    }, {
        "id": "54665x0y9",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 0,
            "topLeftY": 9
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x0y8",
            "approachable": 1
        }, {
            "neighborId": "54665x1y9",
            "approachable": 1
        }]
    }, {
        "id": "54665x1y9",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 1,
            "topLeftY": 9
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x0y9",
            "approachable": 1
        }, {
            "neighborId": "54665x2y9",
            "approachable": 1
        }]
    }, {
        "id": "54665x2y9",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 2,
            "topLeftY": 9
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x1y9",
            "approachable": 1
        }, {
            "neighborId": "54665x3y9",
            "approachable": 1
        }]
    }, {
        "id": "54665x3y9",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 3,
            "topLeftY": 9
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x2y9",
            "approachable": 1
        }, {
            "neighborId": "54665x4y9",
            "approachable": 1
        }]
    }, {
        "id": "54665x4y9",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 4,
            "topLeftY": 9
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x3y9",
            "approachable": 1
        }, {
            "neighborId": "54665x5y9",
            "approachable": 1
        }]
    }, {
        "id": "54665x5y9",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 5,
            "topLeftY": 9
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x5y8",
            "approachable": 1
        }, {
            "neighborId": "54665x4y9",
            "approachable": 1
        }]
    }, {
        "id": "54665x9y9",
        "zones": [{
            "widthUnit": 1,
            "heightUnit": 1,
            "topLeftX": 9,
            "topLeftY": 9
        }],
        "accessibility": 1,
        "type": "corridor",
        "entrance": [{
            "neighborId": "54665x9y8",
            "approachable": 1
        }]
    }]
    */

    floorSpaceUnitsFullList=floorSpaceUnits; //adding only the rooms to the full list
    //floorSpaceUnitsFullList -add to each room in dataMap his neighbor list.

    //scanning the map and marking the corridor space units as unique name
    generateCorridorIds();

    for(let i = 0;i<floorWidthVal;i++){
        for(let j = 0;j<floorHeightVal;j++){
            let cell =  document.getElementById("x"+j+"y"+i);
            let type = cell.getAttribute('type');
            //creating a corridor object and adding his  neighbor to the him
            if(type === 'corridor'){
                console.log("cooridor: x="+i+"  y="+j);
                //wil scan the corridor 4 sids and add his neighbors to him and add him to spaceUnits list.
                createCorridorObjects(i,j,cell);
            }
            else {
                //the current cell is spaceUnit entrance
                //will scan the spaceUnit entrance and connect the room entrance neighbor to the roomData in dataMap
                updateSpaceUnitNeighbors(i,j,cell);
            }
        }
    }
    //console.log("f id is:"+floorId);
    floorSpaceUnitsFullList.data=floorSpaceUnits;
    console.log("***the full list of space unit is:**");
    console.log(floorSpaceUnitsFullList);
        $.ajax({
            url: domain+"/api/v1/mapProducer/floor?floorId="+floorId,
            crossDomain:true,
            type: "PUT",
            processData: false,
            contentType: 'application/json; charset=utf-8',
            data:JSON.stringify(floorSpaceUnitsFullList),
            success: function(msg) {
                console.log("saved goood- massage is:");
                console.log(msg);
                alert("Floor successfully saved");

            },
            error:function (error) {
                console.log("EROOR- massage is:");
                console.log(error);
                alert("Error on saving the floor");

            }
        });
}




